﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Numerics;

namespace API.Models
{
    [Table("TB_GENERATE_AKHIR")]
    public class FinalGenerate
    {
        [Key]
        [Required]
        [Column("ID_GENERATE_AKHIR")]
        public int Id { get; set; }
        [Column("ID_PERIODE_SIARAN_PERS")]
        public int? PressReleasePeriodId { get; set; }
        [Column("ID_KOMPONEN")]
        public int? ComponentId { get; set; }
        [Column("NILAI")]
        public Decimal? Value { get; set; }
        [Column("WORDS")]
        public string? Words { get; set; }
        [Column("INPUT_DATE")]
        public DateTime? InputDate { get; set; }
        [Column("INPUT_BY")]
        public string? InputBy { get; set; }
    }
}
