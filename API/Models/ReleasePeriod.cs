﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    [Table("REF_PERIODE_SIARAN")]
    public class ReleasePeriod
    {
        [Key]
        [Required]
        [Column("ID_PERIODE_SIARAN")]
        public int Id { get; set; }
        [Column("PERIODE_SIARAN")]
        public DateTime? Period { get; set; }
        [Column("INPUT_DATE")]
        public DateTime? InputDate { get; set; }
        [Column("INPUT_BY")]
        public string? InputBy { get; set; }
    }
}
