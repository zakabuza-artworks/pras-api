﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    [Table("TB_NILAI")]
    public class KomponenValue
    {
        [Key]
        [Required]
        [Column("ID_NILAI")]
        public int Id { get; set; }
        [Required]
        [Column("ID_PERIODE_SIARAN_PERS")]
        public required int PressReleasePeriodId { get; set; }
        [Column("PERIODE_SIARAN_PERS")]
        public DateTime? PressReleasePeriod { get; set; }
        [Column("ID_KOMPONEN")]
        public int? ComponentId { get; set; }
        public Komponen? Component { get; set; }
        [Column("PERIODE")]
        public DateTime? Period { get; set; }
        [Column("NILAI")]
        public Decimal? Value { get; set; }
        [Column("DATE_START")]
        public DateTime? DateStart { get; set; }
        [Column("DATE_END")]
        public DateTime? DateEnd { get; set; }
        [Column("INPUT_DATE")]
        public DateTime? InputDate { get; set; }
        [Column("INPUT_BY")]
        public string? InputBy { get; set; }

    }
}
