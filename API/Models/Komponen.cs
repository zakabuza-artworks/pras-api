﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    [Table("REF_KOMPONEN")]
    public class Komponen
    {
        [Key]
        [Column("ID_KOMPONEN")]
        public int Id { get; set; }
        [Column("NAMA_KOMPONEN")]
        public string? Name { get; set; }
        [Column("NAMA_LABEL")]
        public string? LabelName { get; set; }
        [Column("KATEGORI_DATA")]
        public string? DataCategory { get; set; }
        [Column("WORDS_UP")]
        public string? WordsUp { get; set; }
        [Column("WORDS_DOWN")]
        public string? WordsDown { get; set; }
        [Column("WORDS_STAGNANT")]
        public string? WordsStagnant { get; set; }
        [Required]
        [Column("DATE_START")]
        public required DateTime StartDate { get; set; }
        [Column("DATE_END")]
        public DateTime? EndDate { get; set; }
        [Column("INPUT_DATE")]
        public DateTime? InputDate { get; set; }
        [Column("INPUT_BY")]
        public string? InputBy { get; set; }

        //public ICollection<KomponenValue>? KomponenValues { get; set; }
    }
}
