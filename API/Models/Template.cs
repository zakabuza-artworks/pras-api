﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    [Table("REF_TEMPLATE")]
    public class Template
    {
        [Key]
        [Required]
        [Column("ID_TEMPLATE")]
        public int Id { get; set; }
        [Column("TEMPLATE_NAME")]
        public string? Name { get; set; }
        [Column("TEMPLATE_WORDS")]
        public string? Words { get; set; }
        [Column("NO_ORDER")]
        public int? OrderNo { get; set; }
        [Column("INPUT_DATE")]
        public DateTime? InputDate { get; set; }
        [Column("INPUT_BY")]
        public string? InputBy { get; set; }
    }
}
