﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    [Table("TB_GENERATE_PARAGRAPH")]
    public class GenerateParagraph
    {
        [Key]
        [Required]
        [Column("ID_GENERATE_PARAGRAPH")]
        public int Id { get; set; }
        [Column("ID_PERIODE_SIARAN_PERS")]
        public int? PressReleasePeriodId { get; set; }
        [Column("COMPILED_WORDS")]
        public string? CompiledWords { get; set; }
        [Column("INPUT_DATE")]
        public DateTime? InputDate { get; set; }
        [Column("INPUT_BY")]
        public string? InputBy { get; set; }
    }
}
