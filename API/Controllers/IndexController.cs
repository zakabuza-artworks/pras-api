﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("/")]
    [ApiController]
    public class IndexController : ControllerBase
    {
        // GET: api/<IndexController>
        [HttpGet]
        public string Get()
        {
            return "{'status':'OK', 'message':'Welcome to PRAS API. Please open /api/[controller]/[action]/[args] for more data acccess.'}";
        }
    }
}
