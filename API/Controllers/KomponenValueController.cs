﻿using API.Data;
using API.Helpers;
using API.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class KomponenValueController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        
        public KomponenValueController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                List<KomponenValue> komponenValues = _db.KomponenValues.ToList();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data found";
                apiResponse.Data = komponenValues;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }            
        }

        [HttpGet]
        public IActionResult Get(int? Id)
        {
            try
            {
                KomponenValue? komponenValue = _db.KomponenValues.Find(Id);

                if (komponenValue == null)
                {
                    return NotFound();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data found";
                apiResponse.Data = komponenValue;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPost]
        public IActionResult Create(KomponenValue komponenValue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.KomponenValues.Add(komponenValue);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Created";
                apiResponse.Data = komponenValue;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPatch]
        public IActionResult Update(KomponenValue komponenValue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.KomponenValues.Update(komponenValue);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Updated";
                apiResponse.Data = komponenValue;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
            
        }

        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            try
            {
                KomponenValue? komponenValue = _db.KomponenValues.Find(Id);

                if (komponenValue == null)
                {
                    return NotFound();
                }

                _db.KomponenValues.Remove(komponenValue);
                _db.SaveChanges();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Deleted";
                apiResponse.Data = komponenValue;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }
    }
}
