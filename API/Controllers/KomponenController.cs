﻿using API.Data;
using API.Helpers;
using API.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class KomponenController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        
        public KomponenController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                List<Komponen> komponens = _db.Komponens.ToList();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data found";
                apiResponse.Data = komponens;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return BadRequest();
            }            
        }

        [HttpPost]
        public IActionResult Search(string query)
        {
            try
            {
                List<Komponen> komponens = _db.Komponens.Where(komponens => komponens.Name.Contains(query) || komponens.Id == int.Parse(query)).ToList();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data found";
                apiResponse.Data = komponens;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpGet]
        public IActionResult Get(int? Id)
        {
            try
            {
                Komponen? komponen = _db.Komponens.Find(Id);

                if (komponen == null)
                {
                    return NotFound();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data found";
                apiResponse.Data = komponen;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPost]
        public IActionResult Create(Komponen komponen)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Komponens.Add(komponen);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Created";
                apiResponse.Data = komponen;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPatch]
        public IActionResult Update(Komponen komponen)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Komponens.Update(komponen);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Updated";
                apiResponse.Data = komponen;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
            
        }

        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            try
            {
                Komponen? komponen = _db.Komponens.Find(Id);

                if (komponen == null)
                {
                    return NotFound();
                }

                _db.Komponens.Remove(komponen);
                _db.SaveChanges();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Deleted";
                apiResponse.Data = komponen;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }
    }
}
