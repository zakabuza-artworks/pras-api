﻿using API.Data;
using API.Helpers;
using API.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TemplateController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        
        public TemplateController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                List<Template> templates = _db.Templates.ToList();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Found";
                apiResponse.Data = templates;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }            
        }

        [HttpPost]
        public IActionResult Search(string query)
        {
            try
            {
                List<Template> templates = _db.Templates.Where(templates => templates.Name.Contains(query) || templates.Id == int.Parse(query)).ToList();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Found";
                apiResponse.Data = templates;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpGet]
        public IActionResult Get(int? Id)
        {
            try
            {
                Template? template = _db.Templates.Find(Id);

                if (template == null)
                {
                    return NotFound();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Found";
                apiResponse.Data = template;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPost]
        public IActionResult Create(Template template)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Templates.Add(template);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Created";
                apiResponse.Data = template;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPatch]
        public IActionResult Update(Template template)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Templates.Update(template);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Updated";
                apiResponse.Data = template;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
            
        }

        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            try
            {
                Template? template = _db.Templates.Find(Id);

                if (template == null)
                {
                    return NotFound();
                }

                _db.Templates.Remove(template);
                _db.SaveChanges();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Deleted";
                apiResponse.Data = template;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }
    }
}
