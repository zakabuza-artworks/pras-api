﻿using API.Data;
using API.Helpers;
using API.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GeneratorController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        
        public GeneratorController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpPost]
        public IActionResult Generate(int? Id)
        {
            try
            {
                ReleasePeriod releasePeriod = _db.ReleasePeriods.First(b => b.Id == Id);

                if (releasePeriod == null)
                {
                    return NotFound();
                }

                var executeGenerateAllComponentResponse = _db.Database.ExecuteSql($"EXEC SP_GENERATOR_ALL_COMPONENT @ID_PERIODE_SIARAN_PERS = {releasePeriod.Id}");

                if (executeGenerateAllComponentResponse == 0)
                {
                    return BadRequest();
                }

                var executeGenerateParagraphs = _db.Database.ExecuteSql($"EXEC SP_GENERATOR_FINAL @ID_PERIODE_SIARAN_PERS = {releasePeriod.Id}");

                if (executeGenerateParagraphs == 0)
                {
                    return BadRequest();
                }

                #pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                GenerateParagraph generateParagraph = _db.GenerateParagraphs.Where(b => b.PressReleasePeriodId == releasePeriod.Id).FirstOrDefault();
                #pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.

                if (generateParagraph == null)
                {
                    return BadRequest();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Generated";
                apiResponse.Data = generateParagraph;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpGet]
        public IActionResult GetGeneratedParagraphs(int? Id)
        {
            try
            {
                ReleasePeriod? releasePeriod = _db.ReleasePeriods.Find(Id);

                if (releasePeriod == null)
                {
                    return NotFound();
                }

                GenerateParagraph generateParagraph = _db.GenerateParagraphs.Where(b => b.PressReleasePeriodId == releasePeriod.Id).FirstOrDefault();

                if (generateParagraph == null)
                {
                    return NotFound();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Generated";
                apiResponse.Data = generateParagraph;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpGet]
        public IActionResult GetGeneratedComponents(int? Id)
        {
            try
            {
                ReleasePeriod? releasePeriod = _db.ReleasePeriods.Find(Id);

                if (releasePeriod == null)
                {
                    return NotFound();
                }

                List< KomponenValue > komponenValues = _db.KomponenValues.Where(b => b.PressReleasePeriodId == releasePeriod.Id).ToList();

                if (komponenValues == null)
                {
                    return NotFound();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Generated";
                apiResponse.Data = komponenValues;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }
    }
}
