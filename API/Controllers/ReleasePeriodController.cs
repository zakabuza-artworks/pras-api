﻿using API.Data;
using API.Helpers;
using API.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ReleasePeriodController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        
        public ReleasePeriodController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                List<ReleasePeriod> releasePeriods = _db.ReleasePeriods.ToList();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Found";
                apiResponse.Data = releasePeriods;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }            
        }

        [HttpPost]
        public IActionResult Search(string query)
        {
            try
            {
                List<ReleasePeriod> releasePeriods = _db.ReleasePeriods.Where(releasePeriods => releasePeriods.Period.Equals(query) || releasePeriods.Id == int.Parse(query)).ToList();
                
                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Found";
                apiResponse.Data = releasePeriods;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpGet]
        public IActionResult Get(int? Id)
        {
            try
            {
                ReleasePeriod? releasePeriod = _db.ReleasePeriods.Find(Id);

                if (releasePeriod == null)
                {
                    return NotFound();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Found";
                apiResponse.Data = releasePeriod;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPost]
        public IActionResult Create(ReleasePeriod releasePeriod)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.ReleasePeriods.Add(releasePeriod);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Created";
                apiResponse.Data = releasePeriod;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }

        [HttpPatch]
        public IActionResult Update(ReleasePeriod releasePeriod)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.ReleasePeriods.Update(releasePeriod);
                    _db.SaveChanges();
                }

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Updated";
                apiResponse.Data = releasePeriod;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
            
        }

        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            try
            {
                ReleasePeriod? releasePeriod = _db.ReleasePeriods.Find(Id);

                if (releasePeriod == null)
                {
                    return NotFound();
                }

                _db.ReleasePeriods.Remove(releasePeriod);
                _db.SaveChanges();

                var apiResponse = new ApiResponse();
                apiResponse.StatusCode = 200;
                apiResponse.Message = "Data Deleted";
                apiResponse.Data = releasePeriod;

                return Ok(apiResponse);
            }
            catch (Exception)
            {
                return StatusCode(502);
            }
        }
    }
}
