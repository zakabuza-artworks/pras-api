﻿using API.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;


namespace API.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options){ }
        public DbSet<Komponen> Komponens { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<FinalGenerate> FinalGenerators { get; set; }
        public DbSet<GenerateParagraph> GenerateParagraphs { get; set; }
        public DbSet<KomponenValue> KomponenValues { get; set; }
        public DbSet<ReleasePeriod> ReleasePeriods { get; set; }

        public DbSet<FinalGenerate> FinalGenerates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }
}


